package com.esdp.casapi.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "Users")
@ApiModel(value = "Модель Пользователей")
public class User {
    @Id
    @SequenceGenerator(name = "mySeqGenUser", sequenceName = "mySeqUser", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "mySeqGenUser")
    @Column(name = "user_id")
    private long id;

    @Column
    @Size(min = 2, max = 50)
    private String username;

    @Column
    @Size(min = 8, message = "Password should have 8 or more characters")
//    @Pattern(regexp = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}", message = "пароль легкий")
    private String password;

    @Column
    @Size(min = 2, max = 50)
    private String university;

    @Column
    private String status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;


    public User() {
        super();
    }

    public User(String username, String password, String university, String status) {
        super();
        this.username = username;
        this.password = password;
        this.university = university;
        this.status = status;
    }

    public User(String username, String password, String university, Set<Role> roles, Person person) {
        this.username = username;
        this.password = password;
        this.university = university;
        this.roles = roles;
    }

    public User(String username, String password, Set<Role> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", university='" + university + '\'' +
                ", roles=" + roles +
                '}';
    }
}
