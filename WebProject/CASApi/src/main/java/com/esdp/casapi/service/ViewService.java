package com.esdp.casapi.service;

import com.esdp.casapi.model.View;
import com.esdp.casapi.repository.ViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

@Service
public class ViewService {
    @Autowired
    ViewRepository viewRepo;

    private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin


    public View createView(@RequestBody View view){
        logger.info("view -> {}",viewRepo.save(view));
        return viewRepo.save(view);
    }

    public List<View> getViewList(){
        logger.info("view -> {}",viewRepo.findAll());
        return viewRepo.findAll();
    }

    public View getView(@PathVariable("id") long id) {
        logger.info("view -> {}",viewRepo.findById(id).orElse(new View()));
        return viewRepo.findById(id).orElse(new View());
    }

    public View updateView(@RequestBody View view, @PathVariable long id) {
        View putView = viewRepo.findById(id)
                .map(role_ -> {
                    role_.setId(id);
                    role_.setViewName(view.getViewName());
                    role_.setMethod(view.getMethod());
                    return viewRepo.save(role_);
                })
                .orElseGet(() -> {
                    view.setId(id);
                    return viewRepo.save(view);
                });
        logger.info("view -> {}",putView);
        return putView;
    }
}
