package com.esdp.casapi.config;

import com.esdp.casapi.model.View;
import com.esdp.casapi.repository.ViewRepository;
import com.esdp.casapi.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    MyUserDetailsService userDetailsService;

    @Autowired
    ViewRepository viewRepo;


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);


    }
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry =
                http.csrf().disable().authorizeRequests().regexMatchers("/").permitAll();

        List<View> views = viewRepo.findAll();

        for (View v : views)
        {
            List<String> roles = new ArrayList<>();
            v.getRoles().stream().forEach(r -> roles.add(r.getName()));
            HttpMethod method = HttpMethod.valueOf(v.getMethod());
            registry.regexMatchers(method,v.getViewName()).hasAnyRole(roles.toArray(new String[roles.size()]));
        }

        registry.anyRequest().authenticated().and().httpBasic().and().sessionManagement().disable();
    }
}
