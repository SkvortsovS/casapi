package com.esdp.casapi.service;

import com.esdp.casapi.exception.UserNotRegisteredException;
import com.esdp.casapi.exception.WronUniversityNameException;
import com.esdp.casapi.model.Person;
import com.esdp.casapi.model.User;
import com.esdp.casapi.repository.PersonRepository;
import com.esdp.casapi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    UserRepository userRepo;
    @Autowired
    Convertor convertor;
    @Autowired
    GetCurrentUser getCurrentUser;

    private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin


    public Person createPerson(Person person, Authentication authentication) throws IOException {
        if (person.getUserId()==null) {
            throw new UserNotRegisteredException("User_id не найден, создайте сначала юзера");
        }
        User user = getCurrentUser.getCurrentUser(authentication);
        Person adminPerson = personRepository.findByUserId(user.getId());

        Person toCreate = personRepository.findByUserId(person.getUserId());


        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))){
            return updatePerson(person, authentication);
        }
        if (adminPerson.getUniversityId() != toCreate.getUniversityId()) {
            throw new WronUniversityNameException("Этот пользователь не из вашего университета");
        }


        return updatePerson(person, authentication);
    }

    public String uploadPhoto(MultipartFile photo, Authentication authentication) throws IOException {

        User user = getCurrentUser.getCurrentUser(authentication);
        Person person = personRepository.findByUserId(user.getId());

        byte[] bytes = photo.getBytes();

        System.out.println(user);
        person.setPhoto(convertor.convertToBase64(photo));
        personRepository.save(person);

        return "Uploaded!";
    }

    public Person readPerson(Long id, Authentication authentication) {
//        logger.info("person -> {}", personRepository.findById(id).orElse(new Person()));
        User user = getCurrentUser.getCurrentUser(authentication);
        Person person = personRepository.findByUserId(user.getId());
        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))){
            return personRepository.findById(id).get();
        }
        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("ADMIN"))){
            return personRepository.findByIdAndUniversityId(id, person.getUniversityId());
        }
        else{
            return person;
        }


    }

    public List<Person> getPersonList(Authentication authentication) {
//        logger.info("person -> {}", personRepository.findAll());
        User user = getCurrentUser.getCurrentUser(authentication);
        Person person = personRepository.findByUserId(user.getId());
        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))){
            return personRepository.findAll();
        }

        return personRepository.findByUniversityId(person.getUniversityId());
    }


    public Person updatePerson(Person person, Authentication authentication) {
        if (person.getUserId()==null) {
            throw new UserNotRegisteredException("User_id не найден, создайте сначала юзера");
        }
        User admin = getCurrentUser.getCurrentUser(authentication);
        User toUpdatePerson = userRepo.findById(person.getUserId()).get();
        String adminUniversity = admin.getUniversity();
        String userUniversity = toUpdatePerson.getUniversity();

        if (admin.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN")) || adminUniversity.equals(userUniversity)) {
            Person putPerson = personRepository.findById(person.getUserId())
                    .map(person_ -> {

                        person_.setStatus(person.getStatus());
                        person_.setPersonalNumber(person.getPersonalNumber());
                        person_.setName(person.getName());
                        person_.setSurname(person.getSurname());
                        person_.setMiddleName(person.getMiddleName());
                        person_.setFaculty(person.getFaculty());
                        person_.setDepartment(person.getDepartment());
                        person_.setType(person.getType());
                        person_.setRegisteredDate(person.getRegisteredDate());
                        return personRepository.save(person_);
                    }).get();
            return  putPerson;
        } else {
            throw new WronUniversityNameException("Неправильное название университета!");
        }
    }
}


