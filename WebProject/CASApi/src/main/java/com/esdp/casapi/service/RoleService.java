package com.esdp.casapi.service;

import com.esdp.casapi.model.Role;
import com.esdp.casapi.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin

    public Role createRole(@RequestBody Role role){
        logger.info("role -> {}", roleRepository.save(role));
        return roleRepository.save(role);
    }

    public List<Role> getRoleList(){
        logger.info("role -> {}", roleRepository.findAll());
        return roleRepository.findAll();
    }

    public Role getRole(@PathVariable("id") long id) {
        logger.info("role -> {}", roleRepository.findById(id).orElse(new Role()));
        return roleRepository.findById(id).orElse(new Role());
    }

    public Role updateRole(@RequestBody Role role, @PathVariable long id) {
        Role putRole = roleRepository.findById(id)
                .map(role_ -> {
                    role_.setId(id);
                    role_.setName(role.getName());
                    role_.setDescription(role.getDescription());
                    return roleRepository.save(role_);
                })
                .orElseGet(() -> {
                    role.setId(id);
                    return roleRepository.save(role);
                });

        logger.info("role -> {}", putRole);
        return putRole;
    }
}
