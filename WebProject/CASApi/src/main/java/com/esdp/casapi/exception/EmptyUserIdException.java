package com.esdp.casapi.exception;

public class EmptyUserIdException extends RuntimeException{
    public EmptyUserIdException(String message){
        super(message);
    }
}
