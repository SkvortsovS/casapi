package com.esdp.casapi.controller;

import com.esdp.casapi.model.Person;
import com.esdp.casapi.repository.PersonRepository;
import com.esdp.casapi.repository.UserRepository;
import com.esdp.casapi.service.Convertor;
import com.esdp.casapi.service.PersonService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class PersonController {
    @Autowired
    private PersonService personService;
    @Autowired
    UserRepository userRepo;
    @Autowired
    PersonRepository personRepo;
    @Autowired
    Convertor convertor;


    @PostMapping("/person")
    @ApiOperation(value = "Добавление новой записи Person")
    public Person createPerson(@Valid @RequestBody Person person, Authentication authentication) throws IOException {
        return personService.createPerson(person, authentication);

    }

    @PostMapping(value = "/uploadPhoto", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Загрузка фото")
    public String  uploadPhoto(@RequestBody MultipartFile photo, Authentication authentication) throws IOException {

        return personService.uploadPhoto(photo,authentication);
    }


    @GetMapping("/person/{id}")
    @ApiOperation(value = "Просмотр записи  Person по id")
    public Person readPerson(@PathVariable("id") Long id, Authentication authentication) {
        return personService.readPerson(id, authentication);
    }

    @GetMapping("/person")
    @ApiOperation(value = "Просмотр всех записей Person")
    public List<Person> getPersonList(Authentication authentication) {
        return personService.getPersonList(authentication);
    }


    @PutMapping("/person")
    @ApiOperation(value = "Изменение записи Person по id")
    public Person updatePerson(@RequestBody Person person, Authentication authentication) {

        return personService.updatePerson(person, authentication);
    }
}
