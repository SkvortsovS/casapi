package com.esdp.casapi.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import java.util.Set;

@Entity
@ApiModel(value = "Модель Страниц")
public class View {
    @Id
    @GeneratedValue
    @Column(name = "view_id")
    private long id;
    @Column
    private String viewName;
    @Column
    private String method;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "view_role", joinColumns = @JoinColumn(name = "view_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getViewName() {
        return viewName;
    }
    public void setViewName(String viewName) {
        this.viewName = viewName;
    }
    public Set<Role> getRoles() {
        return roles;
    }
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    public String getMethod() {
        return method;
    }
    public void setMethod(String method) {
        this.method = method;
    }

    public View(String viewName, String method, Set<Role> roles) {
        this.viewName = viewName;
        this.method = method;
        this.roles = roles;
    }

    public View() {
        super();
    }

    @Override
    public String toString() {
        return "View{" +
                "id=" + id +
                ", viewName='" + viewName + '\'' +
                ", method='" + method + '\'' +
                ", roles=" + roles +
                '}';
    }
}
