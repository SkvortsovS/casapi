package com.esdp.casapi.controller;

import com.esdp.casapi.model.Role;
import com.esdp.casapi.service.RoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class RoleController
{
    @Autowired
    private RoleService roleService;

    @PostMapping("/role")
    @ApiOperation(value = "Добавление новой записи Role")
    public Role createRole(@RequestBody Role role){
        return roleService.createRole(role);
    }

    @GetMapping("/role")
    @ApiOperation(value = "Просмотр всех записей Role")
    public List<Role> getRoleList(){
        return roleService.getRoleList();
    }
    
    @GetMapping("/role/{id}")
    @ApiOperation(value = "Просмтор записи Role по id")
    public Role getRole(@PathVariable("id") long id) {
        return roleService.getRole(id);
    }

    @PutMapping("/role/{id}")
    @ApiOperation(value = "Изменение записи Role по id")
    public Role updateRole(@RequestBody Role role, @PathVariable long id) {

        return roleService.updateRole(role, id);
    }
}
