package com.esdp.casapi.repository;

import com.esdp.casapi.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository  extends JpaRepository<Person,Long > {
        Person findByUserId(Long id);
        List<Person> findByUniversityId(Long id);
        Person findByIdAndUniversityId(Long id, Long univerId);
        @Modifying
        @Query(value = "update person  set status = 'DELETED' where user_id = ?1",nativeQuery = true)
        void changeStatusToDeleted(long id);
}
