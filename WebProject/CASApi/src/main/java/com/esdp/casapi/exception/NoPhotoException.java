package com.esdp.casapi.exception;


public class NoPhotoException extends RuntimeException{
    public NoPhotoException(String message){
        super(message);
    }
}
