package com.esdp.casapi.repository;

import com.esdp.casapi.model.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {
        University findByName(String name);
}
