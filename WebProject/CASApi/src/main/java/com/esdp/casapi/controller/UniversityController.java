package com.esdp.casapi.controller;

import com.esdp.casapi.model.University;
import com.esdp.casapi.service.UniversityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class UniversityController {
    @Autowired
    private UniversityService universityService;

    @PostMapping("/university")
    @ApiOperation(value = "Добавление записи University")
    public University createUniversity(@Valid @RequestBody University university) throws IOException {
        return universityService.createUniversity(university);
    }

    @GetMapping("/university/{id}")
    @ApiOperation(value = "Просмотр записи University по id")
    public University readUniversity(@PathVariable("id") Long id) {
        return universityService.readUniversity(id);
    }

    @GetMapping("/university")
    @ApiOperation(value = "Просмотр всех записей University")
    public List<University> getUniversityList() {
        return universityService.getUniversityList();
    }

 

    @PutMapping("/university")
    @ApiOperation(value = "Изменение записи University по id")
    public University replaceUniversity(@RequestBody University university) {

        return universityService.replaceUniversity(university);
    }
}
