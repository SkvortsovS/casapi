package com.esdp.casapi.service;

import com.esdp.casapi.model.University;
import com.esdp.casapi.repository.UniversityRepository;
import com.esdp.casapi.repository.ViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;

@Service
public class UniversityService {
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    Convertor convertor;

    private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin


    public University createUniversity(@Valid @RequestBody University university) throws IOException {

        logger.info("university -> {}", universityRepository.save(university));
        return universityRepository.save(university);
    }

    public University readUniversity(@PathVariable("id") Long id) {
        logger.info("university -> {}", universityRepository.findById(id).orElse(new University()));
        return universityRepository.findById(id).orElse(new University());
    }

    public List<University> getUniversityList() {
        logger.info("university -> {}", universityRepository.findAll());
        return universityRepository.findAll();
    }


    public University replaceUniversity(@RequestBody University university) {
        University putUniversity = universityRepository.findById(university.getID())
                .map(university_ -> {
                    university_.setStatus(university.getStatus());
                    university_.setUniversityCode(university.getUniversityCode());
                    university_.setName(university.getName());
                    university_.setAddress(university.getAddress());
                    university_.setRegisteredDate(university.getRegisteredDate());
                    return universityRepository.save(university_);
                })
                .orElseGet(() ->
                {
                    university.setID(university.getID());
                    return universityRepository.save(university);
                });
        logger.info("university -> {}", putUniversity);

        return putUniversity;
    }
}
