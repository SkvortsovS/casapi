package com.esdp.casapi.controller;

import com.esdp.casapi.exception.CountOfStudentsIsOver;
import com.esdp.casapi.model.User;
import com.esdp.casapi.repository.UserRepository;
import com.esdp.casapi.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/user")
    @ApiOperation(value = "Добавление новой записи User")
    public User saveUser(@RequestBody User user, Authentication authentication) {

        if (userService.countStudentsOfUniversity(user.getUniversity()) >= 100)
            throw new CountOfStudentsIsOver("Количество студентов указанного университета превышает установленный лимит в системе!");

        return userService.saveUser(user, authentication);
    }

    @GetMapping("/user")
    @ApiOperation(value = "Просмотр всех пользователей")
    public List<User> getUserList(Authentication authentication) {

        return userService.getUserList(authentication);
    }


    @GetMapping("/getUsersOfUniversity/{id}")
    public List<User> getUsersOfUniversity(@PathVariable("id") Long id) {
        return userService.getUsersOfUniversity(id);
    }

    @GetMapping("/user/{id}")
    @ApiOperation(value = "Просмотр записи  User по id")
    public User getUser(@PathVariable("id") long id, Authentication authentication) {

        return userService.getUser(id, authentication);
    }

    @PutMapping("/user")
    @ApiOperation(value = "Изменение записи User по id")
    public User updateUser(@RequestBody User user, Authentication authentication) {
        return userService.setUser(user, authentication );
    }

    @DeleteMapping("/user/{id}")
    @ApiOperation(value = "Удаление записи User по id")
    public void updateUser(@PathVariable("id") long id){
        userService.deleteById(id);
    }

}
