package com.esdp.casapi.exception;

public class CountOfStudentsIsOver extends RuntimeException{
    public CountOfStudentsIsOver(String message){
        super(message);
    }
}
