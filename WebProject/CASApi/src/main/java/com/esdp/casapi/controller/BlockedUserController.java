package com.esdp.casapi.controller;

import com.esdp.casapi.model.BlockedUser;
import com.esdp.casapi.service.BlockedUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BlockedUserController {

    @Autowired
    private BlockedUserService blockedUserService;
    /*!!!
    Необходимо в дальнейшем добавить логику, которая будет записывать в объект blockedUser в поле "manager_id"
    кто заблокировал (создал блокировку), на момент создания контроллера  как это сделать еще не понятно!!!
     */
    @PostMapping("/blockedUser")
    @ApiOperation(value = "Добавление новой записи блокировки в базу данных casapi")
    public BlockedUser createBlockedUser(@Valid @RequestBody BlockedUser blockedUser){
        return blockedUserService.createBlockedUser(blockedUser);
    }

    // =====================================Block University by university_id

    @PostMapping("/blockUniversityWithId/{id}")
    public String blockUniversityById(@PathVariable("id") Long id)
    {
        return blockedUserService.blockUniversityWithId(id);
    }

    // =====================================Get blocked user by user_id

    @GetMapping("/getBlockedUserId/{id}")
    public BlockedUser getBlockedUserId(@PathVariable("id") Long id)
    {
        return blockedUserService.getBlockedUserWithUserId(id);
    }

    @GetMapping("/blockedUser/{id}")
    @ApiOperation(value = "Просмотр записи по блокировкам по id")
    public BlockedUser readblockedUser(@PathVariable("id") Long id) {
        return blockedUserService.readblockedUser(id);
    }

    @GetMapping("/blockedUser")
    @ApiOperation(value = "Просмотр записей всех блокировок")
    public List<BlockedUser> getblockedUserList() {
        return blockedUserService.getblockedUserList();
    }

    @PutMapping("/blockedUser/{id}")
    @ApiOperation(value = "Изменение записи блокировки по id")
    public BlockedUser updatePerson(@RequestBody BlockedUser blockedUser, @PathVariable Long id) {
        return blockedUserService.updatePerson(blockedUser, id);
    }
}
