package com.esdp.casapi.model;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "roles")
@ApiModel(value = "Модель Ролей пользователей")
public class Role {
    @Id
    @SequenceGenerator(name = "mySeqGenRole", sequenceName = "mySeqRole", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "mySeqGenRole")
    @Column(name = "role_id")
    private long id;

    @Column
    @NotNull(message = "Name cannot be null")
    @Size(min = 1, max = 50)
    @Pattern(regexp = "[A-Z]*")
    private String name;

    @Column
    @NotNull(message = "Name cannot be null")
    @Size(min = 1, max = 100)
    private String description;


    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Role() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
