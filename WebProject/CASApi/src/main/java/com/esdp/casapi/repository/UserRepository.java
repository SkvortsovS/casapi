package com.esdp.casapi.repository;
import com.esdp.casapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByUniversity(String universityName);

    User findByUsername(String username);

    @Query(value = "select * from users u join Person p on p.user_id=u.user_id join Universities uni on uni.university_id=p.university_id where p.university_id = :uniId",
            nativeQuery = true)
    List<User> getUsersWithUniversityId(@Param("uniId") Long uniId);


    User findByIdAndUniversity(long id , String universityName);

    @Query(value=" SELECT COUNT (users.university) \n" +
            " FROM users \n" +
            " where university = ?1 \n" +
            " GROUP BY users.university ",nativeQuery = true)
    Integer countStudentsOfUniversity (String nameUniversity);

    @Modifying
    @Query(value = "update users  set status = 'DELETED' where user_id = ?1",nativeQuery = true)
    void changeStatusToDeleted(long id);

}



