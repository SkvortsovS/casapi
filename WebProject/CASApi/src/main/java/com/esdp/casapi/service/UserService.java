package com.esdp.casapi.service;

import com.esdp.casapi.exception.WronUniversityNameException;
import com.esdp.casapi.model.Person;
import com.esdp.casapi.model.Role;
import com.esdp.casapi.model.University;
import com.esdp.casapi.model.User;
import com.esdp.casapi.repository.PersonRepository;
import com.esdp.casapi.repository.RoleRepository;
import com.esdp.casapi.repository.UniversityRepository;
import com.esdp.casapi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)
public class UserService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    PersonRepository personRepository;
    @Autowired
    UniversityRepository universityRepository;

    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    GetCurrentUser getCurrentUser;

    private Logger logger = LoggerFactory.getLogger(this.getClass());// для zipkin

    public User saveUser(User user, Authentication authentication) {

//        logger.info("user -> {}",userRepo.save(user));


        User admin = getCurrentUser.getCurrentUser(authentication);

        String adminUniversity = admin.getUniversity();
        String userUniversity = user.getUniversity();

        if (user.getRoles() == null) {
            Set<Role> defaultRoleStudent = new HashSet(Arrays.asList(roleRepository.findByName("STUDENT")));
            user.setRoles(defaultRoleStudent);
        }
        if (admin.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))) {
            user.setPassword(encoder.encode(user.getPassword()));
            User savedUser = userRepo.save(user);
            University university = universityRepository.findByName(user.getUniversity());
            personRepository.save(new Person(savedUser.getId(), university.getID()));
            return savedUser;
        }
        if (adminUniversity.equals(userUniversity)) {
            user.setPassword(encoder.encode(user.getPassword()));
            User savedUser = userRepo.save(user);
            University university = universityRepository.findByName(user.getUniversity());
            personRepository.save(new Person(savedUser.getId(), university.getID()));
            return savedUser;

        } else {
            throw new WronUniversityNameException("Неправильное название университета!");
        }
    }


    public List<User> getUserList(Authentication authentication) {
        logger.info("user -> {}",userRepo.findAll());
        User user = getCurrentUser.getCurrentUser(authentication);

        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))){
            return userRepo.findAll();
        }
            return userRepo.findByUniversity(user.getUniversity());

    }


    public List<User> getUsersOfUniversity(Long id)
    {
        return userRepo.getUsersWithUniversityId(id);
    }


    public User getUser(long id, Authentication authentication) {

        User user = getCurrentUser.getCurrentUser(authentication);

        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN"))){
            return userRepo.findById(id).get();
        }
        if(user.getRoles().stream().anyMatch(role -> role.getName().equals("ADMIN"))) {
            return userRepo.findByIdAndUniversity(id, user.getUniversity());
        }
        else{
            return userRepo.findById(user.getId()).get();
        }
    }

    public User setUser(User user, Authentication authentication){

        User admin = getCurrentUser.getCurrentUser(authentication);

        String adminUniversity = admin.getUniversity();
        String userUniversity = user.getUniversity();

        if (admin.getRoles().stream().anyMatch(role -> role.getName().equals("SUPERADMIN")) || adminUniversity.equals(userUniversity)) {
            User putUser = userRepo.findById(user.getId())
                    .map(user_ -> {
                        user_.setUsername(user.getUsername());
                        user_.setPassword(user.getPassword());
                        return userRepo.save(user_);
                    }).get();
//                logger.info("user -> {}",putUser);
            return putUser;
        } else {
            throw new WronUniversityNameException("Неправильное название университета!");
        }
    }

    public Integer countStudentsOfUniversity(String nameUniversity){
        return userRepo.countStudentsOfUniversity(nameUniversity);
    }

    public void deleteById(long id){
        personRepository.changeStatusToDeleted(id);
         userRepo.changeStatusToDeleted(id);
    }

    public boolean isDeleted(Long id){

        User user = userRepo.findById(id).get();

        if(user.getStatus()!=null&&user.getStatus().equals("DELETED")){
            return true;
        }else{
            return false;
        }

    }
}