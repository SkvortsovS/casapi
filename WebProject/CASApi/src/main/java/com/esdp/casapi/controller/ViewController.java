package com.esdp.casapi.controller;

import com.esdp.casapi.model.View;
import com.esdp.casapi.service.ViewService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
public class ViewController {
    @Autowired
    ViewService viewService;

    @PostMapping("/view")
    @ApiOperation(value = "Добавление новой записи View")
    public View createView(@RequestBody View view){
        return viewService.createView(view);
    }

    @GetMapping("/view")
    @ApiOperation(value = "Просмотр всех записей View")
       public List<View> getRoleList(){
            return viewService.getViewList();
   }

    @GetMapping("/view/{id}")
    @ApiOperation(value = "Просмотр записи View по id")
      public View getView(@PathVariable("id") long id) {
           return viewService.getView(id);
        }

    @PutMapping("/view/{id}")
    @ApiOperation(value = "Изменение записи View по id")
        public View updateView(@RequestBody View view, @PathVariable long id) {
            return viewService.updateView(view, id);
        }
}
