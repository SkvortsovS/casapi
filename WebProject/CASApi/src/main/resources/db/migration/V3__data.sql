delete from view_role;
delete from users_roles;
delete from person;
delete from users;
delete from roles;
delete from view;
delete from universities;

drop sequence hibernate_sequence;
drop sequence my_seq_blocked;
drop sequence my_seq_person;
drop sequence my_seq_role;
drop sequence my_seq_university;
drop sequence my_seq_user;

create sequence hibernate_sequence start 1 increment 1;
create sequence my_seq_blocked start 1 increment 1;
create sequence my_seq_person start 1 increment 1;
create sequence my_seq_role start 1 increment 1;
create sequence my_seq_university start 1 increment 1;
create sequence my_seq_user start 1 increment 1;


insert into universities(university_id,status,university_code,name,address,registered_date,logotype) values(1,'active','0011','BGU','Бишкек, ул. ГдетоТам 5', date '1990-01-01', 'photo1' );
insert into universities(university_id,status,university_code,name,address,registered_date,logotype) values(2,'active','0012','KTU','Бишкек, ул. Тут 6', date '1991-01-01', 'photo2' );
insert into universities(university_id,status,university_code,name,address,registered_date,logotype) values(3,'active','0013','KNU','Бишкек, ул. ГдетоТам 7', date '2000-01-01', 'photo3' );
insert into universities(university_id,status,university_code,name,address,registered_date,logotype) values(4,'active','0014','KRSU','Бишкек, ул. Абдрахманова 199', date '2001-01-01', 'photo4' );
insert into universities(university_id,status,university_code,name,address,registered_date,logotype) values(5,'active','0015','KGUSTA','Бишкек, ул. КараЖыгач', date '2010-01-01', 'photo5' );

insert into roles(role_id,name,description) values(1,'SUPERADMIN','Доступ на все' );
insert into roles(role_id,name,description) values(2,'ADMIN','Может давать роли в причисленном ВУЗе, изменять данные как сотрудника так и студента, добавлять новых сотрудников' );
insert into roles(role_id,name,description) values(3,'EMPLOYEE','Может добавлять, просматривать и изменять данные таблицы студент' );
insert into roles(role_id,name,description) values(4,'STUDENT','может просматривать и изменять(1 раз при регистрации) только свою страницу' );

insert into users(user_id,username,password, university) values(1,'userName1','$2a$10$MG8H3gtzlDlqlpVEuWjDgeRwky57ryDiRf0qabElTRGm2J/MLt47m',null); --password1
insert into users(user_id,username,password, university) values(2,'userName2','$2a$10$3TPHrWFewsKIpxZYXzwg7OJRLLTTInksj2HELyCAgnJgkCjLc3NhK','BGU'); --password2
insert into users(user_id,username,password, university) values(3,'userName3','$2a$10$VGW1kXxieF9q2q1YI0Yaj.4eybSiNOS62MoEnwVhDZUt8I55Fl3YK','BGU');--password3
insert into users(user_id,username,password, university) values(4,'userName4','$2a$10$58lCvumuw.98anuvbJhoF.Ksm.y8sLBA5x6b4lBmI55rT1hVAESU2','BGU');--password4
insert into users(user_id,username,password, university) values(5,'userName5','$2a$10$tgMFYVND/7hmLqe0JqbIBeRSkuW4PMddyp4ctnAsBglfSGWQ6tq/m','BGU');--password5
insert into users(user_id,username,password, university) values(6,'userName6','$2a$10$/DGKwaeyTqRhXyWuIBHhPOJxudZ0upu/of8fMlngwTS7q32Hwmf1u','BGU');--password6
insert into users(user_id,username,password, university) values(7,'userName7','$2a$10$U4OgSgfWgXHrwU/IE6dUXe3.08hnC032IS1T/4En7HmbzYUsRxoUK','BGU');--password7
insert into users(user_id,username,password, university) values(8,'userName8','$2a$10$zxDYA/yqJ4X5/QUHQJ3DRuVpzxtZi.Tx3wPAVUZ7JVUBPkn1OOWHq','KTU');--password8
insert into users(user_id,username,password, university) values(9,'userName9','$2a$10$rGNP7/ZSrpgqUXxMsYEd3OdUirHXEYUOvffkvklne7xLRD2TMWx8.','KTU');--password9
insert into users(user_id,username,password, university) values(10,'userName10','$2a$10$BO.q1k5nPP.Nmsz2aN.49egCgttQCYmgQtMdPPc3waHcYiOPnmBoi','KTU');--password10
insert into users(user_id,username,password, university) values(11,'userName11','$2a$10$fClNpZjBq46.Poxfg0/Uu.Vkud54bS4/52PzGMVBD/./vWS3AmXom','KTU');--password11
insert into users(user_id,username,password, university) values(12,'userName12','$2a$10$BZyedR/l5lf3iFJYy26ILuoVenG3t9B82jbH0IGIVjnmfZq99zL7S','KTU');--password12
insert into users(user_id,username,password, university) values(13,'userName13','$2a$10$sFM6cwRvdih6pe5gmI8kVeQJfwqcA94aL1mDBG4B/OQQGWaHoGyWO','KTU');--password13
insert into users(user_id,username,password, university) values(14,'userName14','$2a$10$OMy44xZ3N7Mv5GhOti17b.oYzWt0ZAdo6xd2zpKW.tQBLZd55REfu','KTU');--password14
insert into users(user_id,username,password, university) values(15,'userName15','$2a$10$dIqj1q29jvQJKDJaKNKxMuG8KSppaPwEgv/MnfOeU3lIbvUr51GyK','KNU');--password15
insert into users(user_id,username,password, university) values(16,'userName16','$2a$10$RP4waeZB44xILwCxpG2HAeSS75w8HUmM.iea4WT7rub2Dutcery7a','KNU');--password16
insert into users(user_id,username,password, university) values(17,'userName17','$2a$10$68XGLCVhNlwjh1DDP25k3eE8ZqLJzcZvwlx6nZFT/LxkFhm/C5jsa','KNU');--password17
insert into users(user_id,username,password, university) values(18,'userName18','$2a$10$ARg5qpOewBChVGmGiEMdAOeDTQUa4/gnpZIYLVospEYKweUOsqGlC','KNU');--password18
insert into users(user_id,username,password, university) values(19,'userName19','$2a$10$pM9rvXHUodd8NomPiLQRYuxGqBF.NsUz/1xYbA/HIFUF7M64PrN8.','KNU');--password19
insert into users(user_id,username,password, university) values(20,'userName20','$2a$10$/nvVjgK4cRj/.UIWXe6u5.HrvceoGCTar1WSdozmEqlx.RY27TZHS','KNU');--password20

alter table person add column university_id bigint;

insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(1,'active','00001','Имя','Фамилия','отчество','Фото1','faculty','department','type', date '2000-01-01', null,1);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(2,'active','00002','Имясы','Фамилиясы','отчествосы','Фото2','faculty','department','type', date '2000-01-01', 1,2);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(3,'active','00003','Имя3','Фамилия3','отчество3','Фото3','faculty','department','type', date '2000-01-01', 1,3);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(4,'active','00004','Имя4','Фамилия4','отчество4','Фото4','faculty','department','type', date '2000-01-01', 1,4);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(5,'active','00005','Имя5','Фамилия5','отчество5','Фото5','faculty','department','type', date '2000-01-01', 1,5);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(6,'active','00006','Имя6','Фамилия6','отчество6','Фото6','faculty','department','type', date '2000-01-01', 1,6);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(7,'active','00007','Имя7','Фамилия7','отчество7','Фото7','faculty','department','type', date '2000-01-01', 1,7);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(8,'active','00008','Имя8','Фамилия8','отчество8','Фото8','faculty','department','type', date '2000-01-01', 2,8);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(9,'active','00009','Имя9','Фамилия9','отчество9','Фото9','faculty','department','type', date '2000-01-01', 2,9);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(10,'active','00010','Имя10','Фамилия10','отчество10','Фото10','faculty','department','type', date '2000-01-01', 2,10);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(11,'active','00011','Имя11','Фамилия11','отчество11','Фото11','faculty','department','type', date '2000-01-01', 2,11);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(12,'active','00012','Имя12','Фамилия12','отчество12','Фото12','faculty','department','type', date '2000-01-01', 2,12);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(13,'active','00013','Имя13','Фамилия13','отчество13','Фото13','faculty','department','type', date '2000-01-01', 2,13);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(14,'active','00014','Имя14','Фамилия14','отчество14','Фото14','faculty','department','type', date '2000-01-01', 2,14);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(15,'active','00015','Имя15','Фамилия15','отчество15','Фото15','faculty','department','type', date '2000-01-01', 3,15);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(16,'active','00016','Имя16','Фамилия16','отчество16','Фото16','faculty','department','type', date '2000-01-01', 3,16);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(17,'active','00017','Имя17','Фамилия17','отчество17','Фото17','faculty','department','type', date '2000-01-01', 3,17);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(18,'active','00018','Имя18','Фамилия18','отчество18','Фото18','faculty','department','type', date '2000-01-01', 3,18);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(19,'active','00019','Имя19','Фамилия19','отчество19','Фото19','faculty','department','type', date '2000-01-01', 3,19);
insert into person(person_id,status,personal_number, name,surname,middle_name,photo,faculty,department,type,registered_date,university_id,user_id) values(20,'active','00020','Имя20','Фамилия20','отчество20','Фото20','faculty','department','type', date '2000-01-01', 3,20);

insert into users_roles(user_id, role_id) values(1,1);
insert into users_roles(user_id, role_id) values(2,2);
insert into users_roles(user_id, role_id) values(3,2);
insert into users_roles(user_id, role_id) values(4,3);
insert into users_roles(user_id, role_id) values(5,3);
insert into users_roles(user_id, role_id) values(6,3);
insert into users_roles(user_id, role_id) values(7,3);
insert into users_roles(user_id, role_id) values(8,3);
insert into users_roles(user_id, role_id) values(9,3);
insert into users_roles(user_id, role_id) values(10,3);
insert into users_roles(user_id, role_id) values(11,4);
insert into users_roles(user_id, role_id) values(12,4);
insert into users_roles(user_id, role_id) values(13,4);
insert into users_roles(user_id, role_id) values(14,4);
insert into users_roles(user_id, role_id) values(15,4);
insert into users_roles(user_id, role_id) values(16,4);
insert into users_roles(user_id, role_id) values(17,4);
insert into users_roles(user_id, role_id) values(18,4);
insert into users_roles(user_id, role_id) values(19,4);
insert into users_roles(user_id, role_id) values(20,4);


insert into view (view_id, view_name,method) values(1, '/user','POST');
insert into view (view_id, view_name,method) values(2, '/user/+','GET');
insert into view (view_id, view_name,method) values(3, '/user','GET');
insert into view (view_id, view_name,method) values(4, '/user','PUT');
insert into view (view_id, view_name,method) values(5, '/university','POST');
insert into view (view_id, view_name,method) values(6, '/university/*','GET');
insert into view (view_id, view_name,method) values(7, '/university','GET');
insert into view (view_id, view_name,method) values(8, '/university','PUT');
insert into view (view_id, view_name,method) values(9, '/role','POST');
insert into view (view_id, view_name,method) values(10, '/role/*','GET');
insert into view (view_id, view_name,method) values(11, '/role','GET');
insert into view (view_id, view_name,method) values(12, '/role','PUT');
insert into view (view_id, view_name,method) values(13, '/person','POST');
insert into view (view_id, view_name,method) values(14, '/person/+','GET');
insert into view (view_id, view_name,method) values(15, '/person','GET');
insert into view (view_id, view_name,method) values(16, '/person','PUT');
insert into view (view_id, view_name,method) values(17, '/view','POST');
insert into view (view_id, view_name,method) values(18, '/view','GET');
insert into view (view_id, view_name,method) values(19, '/view/*','GET');
insert into view (view_id, view_name,method) values(20, '/view','PUT');
insert into view (view_id, view_name,method) values(21, '/blockedUser','POST');
insert into view (view_id, view_name,method) values(22, '/blockedUser/+','GET');
insert into view (view_id, view_name,method) values(23, '/blockedUser','GET');
insert into view (view_id, view_name,method) values(24, '/blockedUser','PUT');
insert into view (view_id, view_name,method) values(25,'/person/.','GET');
insert into view (view_id, view_name,method) values(26,'/user/.','GET');
insert into view (view_id, view_name,method) values(27,'/blockedUser/.','GET');



insert into view_role(VIEW_ID, ROLE_ID) values(1,1);
insert into view_role(VIEW_ID, ROLE_ID) values(2,1);
insert into view_role(VIEW_ID, ROLE_ID) values(3,1);
insert into view_role(VIEW_ID, ROLE_ID) values(4,1);
insert into view_role(VIEW_ID, ROLE_ID) values(5,1);
insert into view_role(VIEW_ID, ROLE_ID) values(6,1);
insert into view_role(VIEW_ID, ROLE_ID) values(7,1);
insert into view_role(VIEW_ID, ROLE_ID) values(8,1);
insert into view_role(VIEW_ID, ROLE_ID) values(9,1);
insert into view_role(VIEW_ID, ROLE_ID) values(10,1);
insert into view_role(VIEW_ID, ROLE_ID) values(11,1);
insert into view_role(VIEW_ID, ROLE_ID) values(12,1);
insert into view_role(VIEW_ID, ROLE_ID) values(13,1);
insert into view_role(VIEW_ID, ROLE_ID) values(14,1);
insert into view_role(VIEW_ID, ROLE_ID) values(15,1);
insert into view_role(VIEW_ID, ROLE_ID) values(16,1);
insert into view_role(VIEW_ID, ROLE_ID) values(17,1);
insert into view_role(VIEW_ID, ROLE_ID) values(18,1);
insert into view_role(VIEW_ID, ROLE_ID) values(19,1);
insert into view_role(VIEW_ID, ROLE_ID) values(20,1);
insert into view_role(VIEW_ID, ROLE_ID) values(21,1);
insert into view_role(VIEW_ID, ROLE_ID) values(22,1);
insert into view_role(VIEW_ID, ROLE_ID) values(23,1);
insert into view_role(VIEW_ID, ROLE_ID) values(24,1);
insert into view_role(VIEW_ID, ROLE_ID) values(25,1);
insert into view_role(VIEW_ID, ROLE_ID) values(25,2);
insert into view_role(VIEW_ID, ROLE_ID) values(25,3);
insert into view_role(VIEW_ID, ROLE_ID) values(25,4);
insert into view_role(VIEW_ID, ROLE_ID) values(26,1);
insert into view_role(VIEW_ID, ROLE_ID) values(26,2);
insert into view_role(VIEW_ID, ROLE_ID) values(26,3);
insert into view_role(VIEW_ID, ROLE_ID) values(26,4);
insert into view_role(VIEW_ID, ROLE_ID) values(27,1);
insert into view_role(VIEW_ID, ROLE_ID) values(27,2);
insert into view_role(VIEW_ID, ROLE_ID) values(27,3);
insert into view_role(VIEW_ID, ROLE_ID) values(27,4);




insert into view_role(VIEW_ID, ROLE_ID) values(1,2);
insert into view_role(VIEW_ID, ROLE_ID) values(2,2);
insert into view_role(VIEW_ID, ROLE_ID) values(3,2);
insert into view_role(VIEW_ID, ROLE_ID) values(4,2);
insert into view_role(VIEW_ID, ROLE_ID) values(13,2);
insert into view_role(VIEW_ID, ROLE_ID) values(14,2);
insert into view_role(VIEW_ID, ROLE_ID) values(15,2);
insert into view_role(VIEW_ID, ROLE_ID) values(16,2);
insert into view_role(VIEW_ID, ROLE_ID) values(21,2);
insert into view_role(VIEW_ID, ROLE_ID) values(22,2);
insert into view_role(VIEW_ID, ROLE_ID) values(23,2);
insert into view_role(VIEW_ID, ROLE_ID) values(24,2);
insert into view_role(VIEW_ID, ROLE_ID) values(13,3);
insert into view_role(VIEW_ID, ROLE_ID) values(15,3);
insert into view_role(VIEW_ID, ROLE_ID) values(22,3);
insert into view_role(VIEW_ID, ROLE_ID) values(23,3);
insert into view_role(VIEW_ID, ROLE_ID) values(13,4);

alter sequence my_seq_user restart with 21;
alter sequence my_seq_person restart with 21;
alter sequence my_seq_role restart with 5;
alter sequence my_seq_university restart with 6;
alter sequence hibernate_sequence restart with 25;

alter table if exists person add constraint uni_id_fk foreign key (university_id) references universities;
alter table if exists users add column status varchar(20);
insert into view values(28,'/user/.','DELETE');
insert into view_role values (28,1);